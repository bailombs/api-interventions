# Intervention API

Une Api d'interventions d'application using Xampp symfony orm doctrine and others

## Prerequisites

- [Php 8](https://www.php.net/manual/fr/intro-whatis.php)
- [Composer](https://getcomposer.org/)
- [Symfony](https://symfony.com/doc/current/setup.html)
- [XAMPP]

## Installation

First of all, clone the project with HTTPS or Clone the repo onto your computer

```bash
  git https://gitlab.com/bailombs/api-interventions.git
```

The second time you need to install dependencies for the Back_end and the Front_end

### Api-interventions

Need to be inside the root of the project folder, and run these commands (install dependencies, and run locally).

> Composer install

```

symfony server:start

Your server should now be running at http://locahost:8000
```

## API Documentation

To learn more about how the API works, once you have started your local environment, you can visit: http://localhost:8000/api/doc

## Dependencies

| Name         |
| ------------ |
| php8         |
| MySQL        |
| Symfony      |
| ORM DOCTRINE |
| Nelmio       |
