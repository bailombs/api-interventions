<?php

namespace App\Controller;

use App\Entity\Intervention;
use App\Repository\InterventionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class InterventionController extends AbstractController
{
    /**
     * Recuperer Toutes les interventions
     * @param InterventionRepository $InterventionRepository   
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    #[Route('/api/interventions', name: 'intervention', methods: ['GET'])]
    public function getAllInterventions(InterventionRepository $interventionRepository, SerializerInterface $serializer): JsonResponse
    {
        $interventionsList = $interventionRepository->findAll();

        $jsonInterventionsList = $serializer->serialize($interventionsList, 'json');

        return new JsonResponse($jsonInterventionsList, Response::HTTP_OK, [], true);
    }



    /**
     * Recuperer  une seule intervention
     * @param Intervention $intervention  
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    #[Route('/api/interventions/{id}', name: 'detailIntervention', methods: ['GET'])]
    public function getDetailBook(Intervention $intervention, SerializerInterface $serializer): JsonResponse
    {
        $jsonIntervention = $serializer->serialize($intervention, 'json');
        return new JsonResponse($jsonIntervention, Response::HTTP_OK, [], true);
    }



    /**
     * Creer  une Intervention
     * @param Request $request 
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    #[Route('/api/interventions', name: 'createIntervention', methods: ['POST'])]
    public function createIntervention(Request $request, SerializerInterface $serializer, EntityManagerInterface $em): JsonResponse
    {
        $intervention = $serializer->deserialize($request->getContent(), Intervention::class, 'json');
        $em->persist($intervention);
        $em->flush();

        $jsonIntervention = $serializer->serialize($intervention, 'json');
        return new JsonResponse($jsonIntervention, Response::HTTP_ACCEPTED, [], true);
    }


    /**
     * Modifier une Intervention
     * @param Intervention $currentIntervention
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    #[Route('/api/interventions/{id}', name: 'updateIntervention', methods: ['PUT'])]
    public function updateIntervention(Intervention $currentIntervention, Request $request, SerializerInterface $serializer, EntityManagerInterface $em): JsonResponse
    {

        $updateIntervention = $serializer->deserialize($request->getContent(), Intervention::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $currentIntervention]);

        $em->persist($updateIntervention);
        $em->flush();

        return new JsonResponse('Intervention Modifiée', Response::HTTP_NO_CONTENT);
    }


    /**
     * Modifier une Intervention
     * @param Intervention $currentIntervention    
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */

    #[Route('/api/interventions/{id}', name: 'deleteIntervention', methods: ['DELETE'])]
    public function deleteIntervention(Intervention $intervention, EntityManagerInterface $em): JsonResponse
    {
        $em->remove($intervention);
        $em->flush();
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
