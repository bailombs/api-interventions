<?php

namespace App\DataFixtures;

use App\Entity\Intervention;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $dataInterventions = [
            [
                'created_at' => '2020-12-12 14:53:09',
                'name' => 'Nid de poule',
                'description' => 'Le nid de poules sur la route des prés devient dangereux. Pourriez-vous intervenir pour le reboucher ?',
                'senderName' => 'Romuald Gauthier',
                'senderEmail' => 'romuald_gautier@gmail.com',
                'senderPhone' => '0648736255'
            ],
            [
                'created_at' => '2020-04-13 08:36:11',
                'name' => 'Fuite d\'eau toilettes',
                'description' => 'Il y a une fuite d\'eau dans les toilettes du 3ème étage au niveau du premier lavabo.',
                'senderName' => 'Lucie Perron',
                'senderEmail' => 'lperron@ecole.ac-nantes.fr',
                'senderPhone' => '0251728449'
            ],
            [
                'created_at' => '2020-05-01 10:00:19',
                'name' => 'Arbre obstrue la piste de VTT',
                'description' => 'Un arbre est tombé suite à la tempète d\'hier et bloque le chemin dans la forêt des Lutins',
                'senderName' => 'Sonia André',
                'senderEmail' => 'sonia-andre@live.com',
                'senderPhone' => '0688376519'
            ],
            [
                'created_at' => '2020-05-04 21:49:57',
                'name' => 'Panneau STOP manquant',
                'description' => 'Panneau STOP manquant',
                'senderName' => 'Philippe Legallet',
                'senderEmail' => 'philippe.legallet@wanadoo.fr',
                'senderPhone' => null
            ]
        ];

        foreach ($dataInterventions as $value) {
            $intervention = new Intervention();
            $intervention->setCreatedAt(new \DateTimeImmutable($value['created_at']));
            $intervention->setName($value['name']);
            $intervention->setDescription($value['description']);
            $intervention->setSenderName($value['senderName']);
            $intervention->setSenderEmail($value['senderEmail']);
            $intervention->setSenderPhone($value['senderPhone']);
            $manager->persist($intervention);
        }



        $manager->flush();
    }
}
